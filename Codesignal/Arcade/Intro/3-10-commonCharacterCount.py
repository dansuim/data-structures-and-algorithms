"""
Given two strings, find the number of common characters between them.

Example

For s1 = "aabcc" and s2 = "adcaa", the output should be
solution(s1, s2) = 3.

Strings have 3 common characters - 2 "a"s and 1 "c".
"""


def solution(s1, s2):

    # trying to solve by going through each character and using a while loop
    # to break once you have a match
    # same = 0
    # for i in range(0, len(s1 - 1)):
    #     breakLoop = 0
    #     while breakLoop < 1:
    #         for j in s2:
    #             if j == s1[i]:
    #                 same += 1
    #                 breakLoop += 1
    #     breakLoop -= 1
    # return same

    common_characters = 0
    char_count_s2 = {}  # Dictionary to store character counts of s2

    for char in s2:
        char_count_s2[char] = char_count_s2.get(char, 0) + 1

    for char in s1:
        if char in char_count_s2 and char_count_s2[char] > 0:
            common_characters += 1
            char_count_s2[char] -= 1

    return common_characters


def solution(s1, s2):
    com = [min(s1.count(i),s2.count(i)) for i in set(s1)]
    return sum(com)

def solution(s1, s2):
    return sum(min(s1.count(x), s2.count(x)) for x in set(s1))
