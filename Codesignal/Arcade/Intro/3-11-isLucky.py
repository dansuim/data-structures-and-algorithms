"""
Ticket numbers usually consist of an even number of digits. A ticket number is considered lucky if the sum of the first half of the digits is equal to the sum of the second half.

Given a ticket number n, determine if it's lucky or not.

Example

For n = 1230, the output should be
solution(n) = true;
For n = 239017, the output should be
solution(n) = false.
"""


def solution(n):

    instaList = [int(x) for x in str(n)]
    # print(instaList)
    midpoint = int(len(instaList) / 2)
    # print(midpoint)
    firstn = instaList[0:midpoint]
    # print(firstn)
    secondn = instaList[midpoint:]
    # print(secondn)
    return sum(firstn) == sum(secondn)

def solution(n):
    s = [int(x) for x in str(n)]
    l = int(len(s)/2)
    return sum(s[:l]) == sum(s[l:])


def solution(n):
    s = str(n)
    pivot = len(s)//2
    left, right = s[:pivot], s[pivot:]
    return sum(map(int, left)) == sum(map(int, right))
