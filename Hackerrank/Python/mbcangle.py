# ABC is a right triangle, 90 at B.
# Therefore, ABC = 90 .

# Point M is the midpoint of hypotenuse .

# You are given the lengths AB and BC.
# Your task is to find MBC (angle O, as shown in the figure) in degrees.

# Input Format

# The first line contains the length of side AB.
# The second line contains the length of side BC.

# Constraints
# 0 < AB <= 100
# 0 <BC <= 100

# Lengths AB and BC are natural numbers.
# Output Format

# Output MBC in degrees.

# Note: Round the angle to the nearest integer.

# Examples:
# If angle is 56.5000001°, then output 57°.
# If angle is 56.5000000°, then output 57°.
# If angle is 56.4999999°, then output 56°.
# 0 < o < 90

# Sample Input

# 10
# 10
# Sample Output

# 45°


################################################################
# 1
# Enter your code here. Read input from STDIN. Print output to STDOUT
from math import degrees, atan2

AB = float(input())
BC = float(input())

MBC = round(degrees(atan2(AB, BC)))
print((str(MBC)), chr(176), sep='')


################################################################

# 2
# importing the module
import math

# taking the input from user
ab = float(input())
bc = float(input())

# finding the distance
ac = math.sqrt((ab*ab)+(bc*bc))
bm = ac / 2.0
mc = bm

# equalizing the sides
b = mc
c = bm
a = bc

# where b=c
# finding the angle in radian
angel_b_radian = math.acos(a / (2*b))

# converting the radian to degree
angel_b_degree = int(round((180 * angel_b_radian) / math.pi))

# printing with degree
print(angel_b_degree,'\u00B0',sep='')



########################################################################

#3
# importing math
import math

# taking inputs
a = int(input())
b = int(input())

# finding distace and angle
M = math.sqrt(a**2+b**2)
theta = math.acos(b/M )

# printing the angle
print(int(round(math.degrees(theta),0)),'\u00B0',sep='')




########################################################################

# importing math
import math

# taking input from user
AB,BC=int(input()),int(input())

# calculating distance
hype=math.hypot(AB,BC)

# calculating the angle
res=round(math.degrees(math.acos(BC/hype)))

# the 176 represents the degree symbol
degree=chr(176)
print(res,degree, sep='')
