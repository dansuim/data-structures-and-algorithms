# Python program to
# compute sum of digits in
# number.

# Function to get sum of digits
# Method-1: Using str() and int() methods.: The str() method is used to convert the number to string.
    # The int() method is used to convert the string digit to an integer.
    # Convert the number to string and iterate over each digit in the string and after converting each
    # digit to integer and add to the sum of the digits in each iteration.
def getSum1(n):
    sum = 0
    for digit in str(n):
        sum += int(digit)
    return sum


# Function to get sum of digits
# Method-2: Using sum() methods.: The sum() method is used to sum of numbers in the list.
    # Convert the number to string using str() and strip the string and
    # convert to list of number using strip() and map() method resp. Then find the sum using the sum() method.
def getSum2(n):

    strr = str(n)
    list_of_number = list(map(int, strr.strip()))
    return sum(list_of_number)




# Function to get sum of digits
# Method-3: Using General Approach:
    # Get the number
    # Declare a variable to store the sum and set it to 0
    # Repeat the next two steps till the number is not 0
    # Get the rightmost digit of the number with help of remainder ‘%’ operator by dividing it with 10 and add it to sum.
    # Divide the number by 10 with help of ‘//’ operator
    # Print or return the sum
# A. Iterative Approach:
def getSumIterative(n):

    sum = 0
    while (n != 0):

        sum = sum + (n % 10)
        n = n//10

    return sum


# Python program to compute  sum of digits in number.
# Method-3: Using General Approach:
    # Get the number
    # Declare a variable to store the sum and set it to 0
    # Repeat the next two steps till the number is not 0
    # Get the rightmost digit of the number with help of remainder ‘%’ operator by
    # dividing it with 10 and add it to sum.
    # Divide the number by 10 with help of ‘//’ operator
    # Print or return the sum
# B. Recursive Approach:

def sumDigitsRecursive(n):
    return 0 if n == 0 else int(n % 10) + sumDigitsRecursive(int(n / 10))


# Driver code
n = 12345
print(getSum1(n))
print(getSum2(n))
print(getSumIterative(n))
print(sumDigitsRecursive(n))
