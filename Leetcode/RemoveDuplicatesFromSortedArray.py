"""
Remove Duplicates from Sorted Array
Easy

Given an integer array nums sorted in non-decreasing order,
remove the duplicates in-place such that each unique element appears only once.
The relative order of the elements should be kept the same.
Then return the number of unique elements in nums.

Consider the number of unique elements of nums to be k, to get accepted,
you need to do the following things:

Change the array nums such that the first k elements of nums contain the
unique elements in the order they were present in nums initially.
The remaining elements of nums are not important as well as the size of nums.
Return k.
Custom Judge:

The judge will test your solution with the following code:

int[] nums = [...]; // Input array
int[] expectedNums = [...]; // The expected answer with correct length

int k = removeDuplicates(nums); // Calls your implementation

assert k == expectedNums.length;
for (int i = 0; i < k; i++) {
    assert nums[i] == expectedNums[i];
}
If all assertions pass, then your solution will be accepted.

Example 1:

Input: nums = [1,1,2]
Output: 2, nums = [1,2,_]
Explanation: Your function should return k = 2, with the first two elements
of nums being 1 and 2 respectively.
It does not matter what you leave beyond the returned k
(hence they are underscores).
Example 2:

Input: nums = [0,0,1,1,1,2,2,3,3,4]
Output: 5, nums = [0,1,2,3,4,_,_,_,_,_]
Explanation: Your function should return k = 5, with the first five
elements of nums being 0, 1, 2, 3, and 4 respectively.
It does not matter what you leave beyond the returned k
(hence they are underscores).

Constraints:

1 <= nums.length <= 3 * 104
-100 <= nums[i] <= 100
nums is sorted in non-decreasing order.
"""


# Two-pointer approach to remove duplicate values in the array in-place.
# The idea is to iterate through the array, and for each encountered element,
# check if it is different from the previous element.
# If it is different, update the array in-place.
class Solution:
    def removeDuplicates(self, nums):
        # nums: List[int]) -> int
        # Edge case for empty array
        if not nums:
            return 0

        k = 1  # Initialize unique element count

        # The loop starts from index 1 (i = 1) because the element at
        # index 0 (i = 0) is considered unique by default.
        for i in range(1, len(nums)):
            # For each element at index i, it checks whether it is different
            # from the previous element at index i - 1.
            # If nums[i] is not equal to nums[i - 1], it means a new unique
            # element is found.
            if nums[i] != nums[i - 1]:
                # In such a case, the value at index i is copied to the
                # next available position determined by the pointer k.
                nums[k] = nums[i]
                # The pointer k is then incremented to the next position.
                k += 1
            # This process continues, and the loop effectively
            # overwrites the array with unique elements.
        return k
    # After the loop completes, the variable k holds the count of unique
    # elements, and the modified array contains those unique elements in the
    # order they were initially present. This two-pointer approach allows the
    # removal of duplicates in-place, with a time complexity of O(n),
    # where n is the length of the array.
