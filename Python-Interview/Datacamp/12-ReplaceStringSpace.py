"""
How to substitute spaces with a specified character in Python?

This is a straightforward string manipulation task where spaces in a given
string need to be replaced with a specific character.

Example 1: If the user provides the string "l vey u" and the character "o",
the output will be "loveyou".

Example 2: If the user provides the string "D t C mpBl ckFrid yS le" and
the character "a", the output will be "DataCampBlackFridaySale".

In the str_replace() function, each letter in the string is iterated over,
and if it is a space, it is replaced with the character specified by the user.
The modified string is then returned.

This function, str_replace(), effectively replaces spaces in the given
string with the specified character, producing the desired output.
"""


def str_replace(text, ch):
    # Set up empty string
    result = ''
    # iterate through text
    for i in text:
        # find desired value
        if i == ' ':
            # perform desired swap of ch
            i = ch
        # add to output string
        result += i
    return result


text = "D t C mpBl ckFrid yS le"
ch = "a"

str_replace(text, ch)
# Output: 'DataCampBlackFridaySale'

"""
By placement in string
"""
my_text = 'Aello world'
my_text = my_text.replace(my_text[0], 'H')
print(my_text)


"""
By the character value
"""
mytext = 'Hello Zorld'
mytext = mytext.replace('Z', 'W')
print(mytext)


# In example function
def replace_in_str(text, ch):
    astring = text.replace(' ', ch)
    return astring

# def str_replace(text, ch):
#     result = ''
#     for i in text:
#         if i == '':
#             i = ch
#         result += i
#     return result

# def replace_in_str(text, ch):
#     astring = text.replace(' ', ch)
#     return astring
