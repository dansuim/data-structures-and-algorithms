"""
Explain List, Dictionary, and Tuple comprehension
"""

# List
"""
List comprehension provides a concise syntax for generating a new list
based on existing values, offering a streamlined alternative to using a for
loop that might span multiple lines and become intricate.
The following example illustrates the simplicity of list comprehension:
"""
my_list = [i for i in range(1, 10)]
my_list
# [1, 2, 3, 4, 5, 6, 7, 8, 9]


# Dictionary
"""
Similar to list comprehension, you can efficiently create a dictionary with
a single line of code. The operation is enclosed within curly brackets {}.
Here's an example:
"""
my_dict = {i for i in range(1, 10)}
my_dict
# {1, 2, 3, 4, 5, 6, 7, 8, 9}


# Tuple
"""
Tuple comprehension is a bit distinct. Utilizing round brackets (),
it generates a generator object rather than a tuple comprehension.
To access the elements, you can either iterate through the generator or
convert it to a list. Here's an example:
"""
my_tuple = (i for i in range(1, 10))
my_tuple
# <generator object <genexpr> at 0x7fb91b151430>
