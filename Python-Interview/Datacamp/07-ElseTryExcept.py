"""
Why include else in the try/except construct in Python?

While try: and except: are commonly used for handling exceptions in Python,
the else: clause serves a distinct purpose—it executes when
no exception is raised.

Example:

To illustrate the use of else:, consider the following examples:

In the first attempt, we provided 2 as the numerator and "d" as the
denominator, resulting in an incorrect input and triggering the except:
block with the message "Invalid input."
In the second attempt, we input 2 as the numerator and 1 as the denominator,
yielding a successful division result of 2. Since no exception occurred,
the else: block is executed, printing the message "Division is successful."
In summary, the else: clause provides a way to execute code specifically
when no exceptions occur in the try: block, contributing to more fine-grained
control in exception handling.
"""


def sample(self, numerator, denominator):
    try:
        num1 = int(input('Enter Numerator: '))
        num2 = int(input('Enter Denominator: '))
        division = num1 / num2
        print(f'Result is: {division}')
    except:
        print('Invalid input!')
    else:
        print('Division is successful.')

# Try 1
    # Enter Numerator: 2
    # Enter Denominator: d
    # Invalid input!

# Try 2
    # Enter Numerator: 2
    # Enter Denominator: 1
    # Result is: 2.0
    # Division is successful.
