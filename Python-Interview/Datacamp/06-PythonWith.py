"""
What is the purpose of the Python "with" statement?

The Python with statement is designed for streamlined exception handling,
enhancing code cleanliness and simplicity. It is commonly employed for
the efficient management of shared resources, such as creating, editing,
and saving files.

Example:

To illustrate, rather than composing multiple lines involving open, try,
finally, and close statements, the with statement simplifies the creation and
writing of a text file:
This concise syntax ensures proper resource management, automatically handling
exceptions and closing the file, making the code more readable and efficient.
"""
# Using the with statement
with open('myfile.txt', 'w') as file:
    file.write('DataCamp Black Friday Sale!!!')
