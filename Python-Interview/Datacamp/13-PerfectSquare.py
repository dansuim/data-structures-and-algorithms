"""
Given a positive integer num, create a function that returns True
if num is a perfect square, otherwise False. The solution involves the
following steps:

Calculate the square root of the number and convert it into an integer.
Square the integer obtained and check if it equals the original number.
Return the result as a boolean.
Test 1:
For the provided number 10 in the valid_square() function:

The square root of 10 is approximately 3.1622776601683795.
Converting it to an integer gives 3.
Squaring 3 results in 9, which is not equal to the original number.
Therefore, the function returns False.
Test 2:
For the provided number 36 in the valid_square() function:

The square root of 36 is 6.
Converting it to an integer gives 6.
Squaring 6 results in 36, which is equal to the original number.
Therefore, the function returns True.
This function efficiently determines whether a given
number is a perfect square.
"""


def valid_square(num):
    # find the square root
    square_root = int(num**0.5)
    # verify if num is the square of previous square root
    check = square_root**2 == num
    return check


valid_square(10)
# Output: False

valid_square(36)
# Output: True

# def valid_square(num):
#     square_root = int(num**0.5)
#     check = square_root**2 == num
#     return check

# import math
# def valid_square(num):
#     check = math.sqrt(num**2) == num
