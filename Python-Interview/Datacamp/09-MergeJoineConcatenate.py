"""
Python Data Science Interview Questions

What is the difference between merge, join and concatenate?

Merge: This operation brings together two DataFrames based on a shared
column identifier. It necessitates two DataFrames, a common column, and
a specification on how the merging should occur. Options include left,
right, outer, inner, and cross joins. By default, it performs an inner join.

Example:

python
Copy code
pd.merge(df1, df2, how='outer', on='Id')
Join: Joining DataFrames relies on their unique index. An optional on
argument can be provided, specifying one or multiple column names. By
default, the join function executes a left join.

Example:

python
Copy code
df1.join(df2)
Concatenate: Concatenation stitches together two or more DataFrames along a
specified axis (rows or columns). Unlike merge and join, it doesn't require
an on argument.

Example:

python
Copy code
pd.concat([df1, df2])
In summary:

join(): Combines DataFrames by index.
merge(): Combines DataFrames by specified column(s).
concat(): Combines DataFrames vertically or horizontally.
"""
