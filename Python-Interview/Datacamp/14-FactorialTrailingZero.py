"""
To determine the number of trailing zeroes in the factorial of an integer n,
follow these steps:

Calculating Factorial:

Iterate over the integers from n to 1 using a while loop, multiplying and
updating the factorial value.
Calculating Trailing Zeros:
2. Convert the factorial number to a string.

Traverse the string from right to left using a loop.
Increment a counter for each "0" encountered until a non-zero digit is found.
Return the count of trailing zeros.
Example:

For factorial_trailing_zeros(10):

The factorial of 10 is 10! = 3,628,800.
The trailing zeroes in 3,628,800 are 2.
Therefore, the function should return 2.
For factorial_trailing_zeros(18):

The factorial of 18 is 18! = 6,402,373,705,728,000.
The trailing zeroes in 6,402,373,705,728,000 are 3.
Therefore, the function should return 3.
This function efficiently calculates the trailing zeros in the
factorial of a given integer.
"""


def factorial_trailing_zeros(n):
    # Calculate factorial
    fact = n
    while n > 1:
        fact *= n - 1
        n -= 1

    # Counter for trailing zeroes
    result = 0

    # Iterate through factorial starting from the end
    for digit in str(fact)[::-1]:
        # If trailing zero, increment the counter
        if digit == "0":
            result += 1
        # If no trailing zero break loop
        else:
            break

    return result


factorial_trailing_zeros(10)
# Output: 2

factorial_trailing_zeros(18)
# Output: 3


# def factorial_trailing_zeros(n):
#     fact = n
#     while n > 1:
#         fact *= n - 1
#         n -= 1

#     result = 0

#     for digit in str(fact)[::-1]:
#         if digit == "0":
#             result += 1
#         else:
#             break
#     return result
