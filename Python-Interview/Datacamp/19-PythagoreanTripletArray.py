"""
Amazon Python interview questions

Write a function that returns True if there is a Pythagorean triplet that
satisfies a2+ b2 = c2

Example:
Input [3, 1, 4, 6, 5]
Output True

Input [10, 4, 6, 12, 5]
Output False

Solution:
Square all the elements in the array.
Sort the array in increasing order.
Run two loops. The outer loop starts from the last index of the array to 1
and the inner loop starts from (outer_loop_index -1) to the start.
Create set() to store the elements between outer loop index
and inner loop index.
Check if there is a number present in the set which is equal to
(array[outerLoopIndex] - array[innerLoopIndex]).
If yes, return True, else False.

This function efficiently checks if there is a Pythagorean triplet in the
given array of integers. It squares all the elements, sorts the array,
and then iterates over the array to find the triplet, returning
True if found, and False otherwise.
"""


def has_pythagorean_triplet(nums):
    # Square all the elements in the array
    nums_squared = [num ** 2 for num in nums]

    # Sort the squared array in increasing order
    nums_squared.sort()

    # Iterate over the array to find the Pythagorean triplet
    n = len(nums_squared)
    for i in range(n - 1, 1, -1):
        # Create a set to store elements between outer loop index and
        # inner loop index
        s = set()
        for j in range(i - 1, -1, -1):
            # Check if there is a number present in the set which satisfies
            # the Pythagorean condition
            if (nums_squared[i] - nums_squared[j]) in s:
                return True
            s.add(nums_squared[j])
    return False


# Example usage
arr1 = [3, 1, 4, 6, 5]
arr2 = [10, 4, 6, 12, 5]

print(has_pythagorean_triplet(arr1))  # Output: True
print(has_pythagorean_triplet(arr2))  # Output: False
