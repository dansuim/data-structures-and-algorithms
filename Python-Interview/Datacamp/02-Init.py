"""
It functions as a constructor within Object-Oriented Programming (OOP)
terminology, playing a pivotal role in initializing the state when a new
object is created. This involves assigning values to object properties or
executing essential operations during object creation.

The init() method is specifically reserved for Python classes, automatically
invoked upon object creation.

Example:

Consider the book_shop class we've developed,
featuring a constructor and a book() function. The constructor stores the
book title, and the book() function prints the book name.

To evaluate our code, we've instantiated the b object with the title "Sandman"
and executed the book() function.
"""


class book_shop:
    # Constructor
    def __init__(self, title):
        self.title = title

    # Sample method
    def book(self):
        print('The title of the book is', self.title)


b = book_shop('Dune')
b.book()
# Output: The title of the book is Dune
