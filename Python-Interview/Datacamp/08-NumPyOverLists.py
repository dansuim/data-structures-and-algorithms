"""
Python Data Science Interview Questions
What are the advantages of NumPy over regular Python lists?

Memory Efficiency:
NumPy arrays are notably more memory-efficient. For instance, creating a list
and a NumPy array with a thousand elements illustrates this
difference: the list consumes 48K bytes, whereas the NumPy array only utilizes
8K bytes of memory.

Speed:
NumPy arrays outperform Python lists in terms of speed. For instance,
when multiplying two lists and two NumPy arrays, each consisting of 1 million
elements, the operation took 0.15 seconds for the list
and 0.0059 seconds for the array.

Versatility:
NumPy arrays offer an array of built-in functionalities, such as simple
array multiplication and addition. In contrast, Python lists lack
the capability to perform basic operations efficiently.

"""
