"""
Amazon Python interview questions

Problem:
We aim to create a function that calculates the number of ways we can make
change with given coin denominations for a specific total amount.

Example:
For instance, given coin denominations [1, 2, 5] and a total amount of 5,
we can make change in five different ways.

Solution:
To achieve this, we'll follow these steps:

1. Create a list of size amount + 1, with additional space for storing the
solution for a zero amount.
2. Initialize the solution list with 1, as there's one way to make change
for a zero amount.
3. Run two loops: the outer loop iterates over the denominations, and the
inner loop runs from the outer loop index to amount + 1.
4. Update the solution array based on different denominations:
solution[i] = solution[i] + solution[i - den].
5. Repeat this process for all elements in the denomination list,
and the last element of the solution list will contain the desired count.
"""


def count_change_ways(coins, total_amount):
    # Create a list of size total_amount + 1 to store the solution
    solution = [0] * (total_amount + 1)
    # Base case: There is one way to make change for zero amount
    solution[0] = 1

    # Iterate over each coin denomination
    for coin in coins:
        # Iterate over the range from coin denomination to total_amount + 1
        for i in range(coin, total_amount + 1):
            # Update the solution for the current amount
            solution[i] += solution[i - coin]

    # Return the number of ways to make change for the total_amount
    return solution[total_amount]


# Example usage
coin_denominations = [1, 2, 5]
total_amount = 5
print(count_change_ways(coin_denominations, total_amount))
# Output: 4
