"""
What is the difference between a mutable data type and an immutable data type?
Mutable Python data types are alterable, allowing modifications and changes
during runtime. Examples of such types include List, Dictionary, and Set.

In contrast, immutable Python data types are unchangeable and remain constant
throughout runtime. Examples include Numeric, String, and Tuple.
"""
