"""
Amazon Python interview questions
You have been given a list of positive integers from 1 to n,
where all numbers from 1 to n are present except for one number,
which we need to find.

Example:

Given the list: [4, 5, 3, 2, 8, 1, 6]
n = 8
missing number = 7

This problem can be solved using a simple mathematical approach:

Find the sum of all elements in the list.
Use the arithmetic series sum formula to calculate the expected
sum of the first n numbers.
Return the difference between the expected sum and the sum of the
elements as the missing number.
"""


def find_missing(input_list):
    # Calculate the sum of elements in the list
    sum_of_elements = sum(input_list)

    # Since there is exactly 1 number missing, the length of the
    # input list + 1 equals the total numbers
    n = len(input_list) + 1

    # Calculate the expected sum using the arithmetic series sum formula
    expected_sum = (n * (n + 1)) // 2

    # Return the difference between the expected sum and the sum of
    # the elements as the missing number
    return expected_sum - sum_of_elements


# Example usage
list_1 = [1, 5, 6, 3, 4]
print(find_missing(list_1))
# Output: 2
