"""
What is monkey patching in Python?

Monkey patching in Python is a dynamic technique that allows altering the
behavior of code at runtime by modifying a class or module during execution.

Example:

To illustrate monkey patching, consider the following example:

We define a class monkey with a patch() function and an external monk_p
function. We then replace the patch function with the monk_p function by
assigning monkey.patch to monk_p. Finally, we test the modification by creating
an object using the monkey class and invoking the patch() function.
Instead of the expected output "patch() is being called," it now displays
"monk_p() is being called."
"""


class monkey:
    def patch(self):
        print("patch() is being called")


def monk_p(self):
    print("monk_p() is being called")


# replacing the address of "patch" with "monk_p"
monkey.patch = monk_p

obj = monkey()

obj.patch()
# Output: monk_p() is being called
# This demonstrates how monkey patching dynamically changes the behavior of a
# class method at runtime.
