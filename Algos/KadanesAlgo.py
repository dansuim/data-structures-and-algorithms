"""
Kadane's Algorithm is an efficient algorithm used to find the maximum subarray sum within a given array of numbers. It solves the problem in linear time complexity, specifically O(n), where n is the length of the array.

Here's how Kadane's Algorithm works:

Initialize two variables: max_so_far and max_ending_here, both set to the first element of the array or to negative infinity if the array contains only negative numbers.

Iterate through the array from the second element onwards:

a. Update max_ending_here by adding the current element to the previous max_ending_here value. This step determines the maximum subarray sum ending at the current position.

b. If max_ending_here becomes negative, reset it to 0. This is because if the sum becomes negative, it would only decrease the sum of any subarray that includes the current element. So, we reset it to start a new subarray.

c. Update max_so_far to the maximum value between max_so_far and max_ending_here. This ensures that max_so_far always holds the maximum subarray sum found so far.

After iterating through the entire array, max_so_far will contain the maximum subarray sum.

Here's an example to illustrate how Kadane's Algorithm works:

Given the array [-2, 1, -3, 4, -1, 2, 1, -5, 4], let's apply Kadane's Algorithm step by step:

Initialize max_so_far and max_ending_here as -2 (first element).

For the second element (1), max_ending_here becomes max(1, 1 + (-2)) = 1, and max_so_far remains -2.

For the third element (-3), max_ending_here becomes max(-3, -3 + 1) = -2, and max_so_far remains -2.

For the fourth element (4), max_ending_here becomes max(4, 4 + (-2)) = 4, and max_so_far becomes max(-2, 4) = 4.

For the fifth element (-1), max_ending_here becomes max(-1, -1 + 4) = 3, and max_so_far remains 4.

For the sixth element (2), max_ending_here becomes max(2, 2 + 3) = 5, and max_so_far becomes max(4, 5) = 5.

For the seventh element (1), max_ending_here becomes max(1, 1 + 5) = 6, and max_so_far becomes max(5, 6) = 6.

For the eighth element (-5), max_ending_here becomes max(-5, -5 + 6) = 1, and max_so_far remains 6.

For the ninth element (4), max_ending_here becomes max(4, 4 + 1) = 5, and max_so_far becomes max(6, 5) = 6.

Finally, the maximum subarray sum is 6.

Kadane's Algorithm is a powerful technique for solving maximum subarray sum problems efficiently and is widely used in various applications, including data analysis, finance, and dynamic programming.



Pseudocode:

Initialize:
    max_so_far = INT_MIN
    max_ending_here = 0

Loop for each element of the array

  (a) max_ending_here = max_ending_here + a[i]
  (b) if(max_so_far < max_ending_here)
            max_so_far = max_ending_here
  (c) if(max_ending_here < 0)
            max_ending_here = 0
return max_so_far

"""
# Python program to find maximum contiguous subarray

# Function to find the maximum contiguous subarray
from sys import maxint


def maxSubArraySum(a, size):

	max_so_far = -maxint - 1
	max_ending_here = 0

	for i in range(0, size):
		max_ending_here = max_ending_here + a[i]
		if (max_so_far < max_ending_here):
			max_so_far = max_ending_here

		if max_ending_here < 0:
			max_ending_here = 0
	return max_so_far

# Driver function to check the above function


a = [-2, -3, 4, -1, -2, 1, 5, -3]

print "Maximum contiguous sum is", maxSubArraySum(a, len(a))

# This code is contributed by _Devesh Agrawal_



"""
Print the Largest Sum Contiguous Subarray
To print the subarray with the maximum sum the idea is to maintain start index of maximum_sum_ending_here at current index so that whenever maximum_sum_so_far is updated with maximum_sum_ending_here then start index and end index of subarray can be updated with start and current index.
Follow the below steps to implement the idea:

Initialize the variables s, start, and end with 0 and max_so_far = INT_MIN and max_ending_here = 0
Run a for loop from 0 to N-1 and for each index i:
Add the arr[i] to max_ending_here.
If max_so_far is less than max_ending_here then update max_so_far to max_ending_here and update start to s and end to i .
If max_ending_here < 0 then update max_ending_here = 0 and s with i+1.
Print values from index start to end.
"""


# Python program to print largest contiguous array sum

from sys import maxsize

# Function to find the maximum contiguous subarray
# and print its starting and end index


def maxSubArraySum(a, size):

    max_so_far = -maxsize - 1
    max_ending_here = 0
    start = 0
    end = 0
    s = 0

    for i in range(0, size):

        max_ending_here += a[i]

        if max_so_far < max_ending_here:
            max_so_far = max_ending_here
            start = s
            end = i

        if max_ending_here < 0:
            max_ending_here = 0
            s = i+1

    print("Maximum contiguous sum is %d" % (max_so_far))
    print("Starting Index %d" % (start))
    print("Ending Index %d" % (end))


# Driver program to test maxSubArraySum
a = [-2, -3, 4, -1, -2, 1, 5, -3]
maxSubArraySum(a, len(a))
