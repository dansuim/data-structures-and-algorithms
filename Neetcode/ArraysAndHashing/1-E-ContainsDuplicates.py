"""
Given an integer array nums, return true if any value appears at least
twice in the array, and return false if every element is distinct.

Example 1:

Input: nums = [1,2,3,1]
Output: true
Example 2:

Input: nums = [1,2,3,4]
Output: false
Example 3:

Input: nums = [1,1,1,3,3,4,3,2,4,2]
Output: true


Constraints:

1 <= nums.length <= 105
-109 <= nums[i] <= 109
"""

# The following goes through each item and iterates through am array.
# and appends an array
# Time limit exceeded on long lists


class Solution:
    def containsDuplicate(self, nums):
        # nums: List[int]) -> bool
        dup = []
        for i in nums:
            if i not in dup:
                dup.append(i)
            else:
                return True
        # DICT REVERSE
        # dup = []
        # for i in nums:
        #     if i in dup:
        #         return True
        #     dup.append(i)


# You can also sort the array and then you only have to iterate through
# the list once
# a hash set will be a time efficient way to run this


class Solution1:
    def containsDuplicate(self, nums):
        # nums: List[int]) -> bool
        hashset = set()
        for i in nums:
            if i in hashset:
                return True
            hashset.add(i)
        return False
